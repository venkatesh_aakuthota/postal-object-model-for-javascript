import PostalAddress from '../../src/postalAddress';
import dummy from '../dummy';

describe('PostalAddress', ()=> {
    describe('constructor', () => {
        it('throws if street is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PostalAddress(
                        null,
                        dummy.city,
                        dummy.iso31662Code,
                        dummy.postalCode,
                        dummy.iso31661Alpha2Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'street required');
        });
        it('sets street', () => {
            /*
             arrange
             */
            const expectedStreet = dummy.streetAddress;

            /*
             act
             */
            const objectUnderTest =
                new PostalAddress(
                    expectedStreet,
                    dummy.city,
                    dummy.iso31662Code,
                    dummy.postalCode,
                    dummy.iso31661Alpha2Code
                );

            /*
             assert
             */
            const actualStreet = objectUnderTest.street;
            expect(actualStreet).toEqual(expectedStreet);

        });
        it('throws if city is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PostalAddress(
                        dummy.streetAddress,
                        null,
                        dummy.iso31662Code,
                        dummy.postalCode,
                        dummy.iso31661Alpha2Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'city required');
        });
        it('sets city', () => {
            /*
             arrange
             */
            const expectedCity = dummy.city;

            /*
             act
             */
            const objectUnderTest =
                new PostalAddress(
                    dummy.streetAddress,
                    expectedCity,
                    dummy.iso31662Code,
                    dummy.postalCode,
                    dummy.iso31661Alpha2Code
                );

            /*
             assert
             */
            const actualCity = objectUnderTest.city;
            expect(actualCity).toEqual(expectedCity);

        });
        it('throws if regionIso31662Code is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PostalAddress(
                        dummy.streetAddress,
                        dummy.city,
                        null,
                        dummy.postalCode,
                        dummy.iso31661Alpha2Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'regionIso31662Code required');
        });
        it('sets regionIso31662Code', () => {
            /*
             arrange
             */
            const expectedRegionIso31662Code = dummy.iso31662Code;

            /*
             act
             */
            const objectUnderTest =
                new PostalAddress(
                    dummy.streetAddress,
                    dummy.city,
                    expectedRegionIso31662Code,
                    dummy.postalCode,
                    dummy.iso31661Alpha2Code
                );

            /*
             assert
             */
            const actualRegionIso31662Code = objectUnderTest.regionIso31662Code;
            expect(actualRegionIso31662Code).toEqual(expectedRegionIso31662Code);

        });
        it('throws if postalCode is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PostalAddress(
                        dummy.streetAddress,
                        dummy.city,
                        dummy.iso31662Code,
                        null,
                        dummy.iso31661Alpha2Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'postalCode required');
        });
        it('sets postalCode', () => {
            /*
             arrange
             */
            const expectedPostalCode = dummy.postalCode;

            /*
             act
             */
            const objectUnderTest =
                new PostalAddress(
                    dummy.streetAddress,
                    dummy.city,
                    dummy.iso31662Code,
                    expectedPostalCode,
                    dummy.iso31661Alpha2Code
                );

            /*
             assert
             */
            const actualPostalCode = objectUnderTest.postalCode;
            expect(actualPostalCode).toEqual(expectedPostalCode);

        });
        it('throws if countryIso31661Alpha2Code is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PostalAddress(
                        dummy.streetAddress,
                        dummy.city,
                        dummy.iso31662Code,
                        dummy.postalCode,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'countryIso31661Alpha2Code required');
        });
        it('sets countryIso31661Alpha2Code', () => {
            /*
             arrange
             */
            const expectedCountryIso31661Alpha2Code = dummy.iso31661Alpha2Code;

            /*
             act
             */
            const objectUnderTest =
                new PostalAddress(
                    dummy.streetAddress,
                    dummy.city,
                    dummy.iso31662Code,
                    dummy.postalCode,
                    expectedCountryIso31661Alpha2Code
                );

            /*
             assert
             */
            const actualCountryIso31661Alpha2Code = objectUnderTest.countryIso31661Alpha2Code;
            expect(actualCountryIso31661Alpha2Code).toEqual(expectedCountryIso31661Alpha2Code);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {

            /*
             arrange
             */
            const objectUnderTest =
                new PostalAddress(
                    dummy.streetAddress,
                    dummy.city,
                    dummy.iso31662Code,
                    dummy.postalCode,
                    dummy.iso31661Alpha2Code
                );

            const expectedObject =
            {
                street: objectUnderTest.street,
                city: objectUnderTest.city,
                regionIso31662Code: objectUnderTest.regionIso31662Code,
                postalCode: objectUnderTest.postalCode,
                countryIso31661Alpha2Code: objectUnderTest.countryIso31661Alpha2Code
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);
        });

    })
});

